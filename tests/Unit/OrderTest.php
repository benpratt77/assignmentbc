<?php

namespace Tests\Unit;

use App\Customer;
use App\Order;
use DateTimeImmutable;
use Tests\TestCase;

class OrderTest extends TestCase
{
    /** @var  Customer $customer1 */
    private $customer1;
    /** @var  Customer $customer2 */
    private $customer2;
    /** @var  Customer $customer3 */
    private $customer3;

    /**
     * Set up an Customers for tests.
     */
    public function setUp()
    {
        parent::setUp();
    }

    public function testLifeTimeValueCustomerWithNoOrders()
    {
        $customer = new Customer(1, 'user', 'one', []);

        $this->assertEquals($customer->getLifeTimeValue(), 0);
    }

    public function testLifeTimeValueCustomerWithSingleOrder()
    {
        $order = new Order(1, new DateTimeImmutable('2018-01-01'), 50.35, 1);
        $customer = new Customer(1, 'Ben', 'Pratt', [$order]);

        $this->assertEquals($customer->getLifeTimeValue(), 50.35);
    }

    public function testLifeTimeValueCustomerWithMultipleOrders()
    {
        $order = new Order(1, new DateTimeImmutable('2018-01-01'), 10, 1);
        $order2 = new Order(2, new DateTimeImmutable('2018-01-01'), 25, 2);
        $customer = new Customer(1, 'user', 'one', [$order, $order2]);

        $this->assertEquals($customer->getLifeTimeValue(), 35);
    }
}
