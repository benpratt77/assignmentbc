#Models
I have created 2 models for this project:

**Customer** to hold the Customer data.

**Order** to hold the individual Order data inside the Customer.

#Services
I created 2 services: the **OrderService** and the **CustomerService**.

These services are responsible for obtaining the data from the Client.

And therefore I have Encapsulated the API SDK so that it can be easily updated and is easier to test.

#Unit Testing
I have provided unit test to test the functionality of getting the lifetime value for a customer.

#Notes
I have made the assumption that lifetime value is the total sum of all orders.

As per instructions I have not made ths look nice, but it hurts. :)

I would have preferred to get all the data for a customers and their data in one request, however I was unable to find a way of doing this and had to then query the data on creation of the new model. This felt somewhat slow and clunky.

I noticed the requests paginate to 50 lines, it seemed out of scope to add pagination for this project. 
