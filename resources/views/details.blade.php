@extends('layouts.app')

@section('title', $customer->getFullName() . "'s Order History")

@section('content')
    <table>
        <thead>
        <tr>
            <th>Date</th>
            <th># of Products</th>
            <th>Total</th>
        </tr>
        </thead>
        <tbody>
        @foreach($customer->getOrders() as $order)
            <tr>
                <td>
                    {{$order->getDate()->format('d-M-Y')}}
                </td>
                <td style="text-align: center;">{{$order->getProductCount()}}</td>
                <td>${{number_format($order->getTotal(),2)}}</td>
            </tr>
        @endforeach
        <tr>
            <td colspan="2">Lifetime Value</td>
            <td>${{ $lifeTimeValue }}</td>
        </tr>
        </tbody>
    </table>
@endsection
