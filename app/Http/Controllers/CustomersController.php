<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\CustomerService;
use Illuminate\Routing\Controller as BaseController;

class CustomersController extends BaseController
{

    protected $customerService;

    public function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
    }

    /**
     * Show a list of 50 Customers.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $customerList = $this->customerService->getCustomers();
        return view('customers', ['customers' => $customerList]);
    }
}
