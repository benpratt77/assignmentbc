<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\CustomerService;
use Illuminate\Routing\Controller as BaseController;

class CustomerDetailsController extends BaseController
{
    protected $customerService;

    public function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
    }

    /**
     * Show the details for a Customer.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(int $id)
    {
        $customer = $this->customerService->getCustomer($id);
        $lifeTimeValue = $customer->getLifeTimeValue();
        return view('details', [
            'customer' => $customer,
            'lifeTimeValue' => $lifeTimeValue,
        ]);
    }
}
