<?php
declare(strict_types=1);

namespace App;

class Customer
{
    /** @var int $id */
    private $id;
    /** @var string $firstName */
    private $firstName;
    /** @var string $lastN0ame */
    private $lastName;
    /**
     * All of the orders this Customer placed
     * @var \Illuminate\Support\Collection $orders
     */
    private $orders;

    /**
     *
     * @param $id
     * @param $firstName
     * @param $lastName
     * @param $orderList
     */
    public function __construct(int $id, string $firstName, string $lastName, array $orderList = [])
    {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->orders = collect();
        if ($orderList) {
            foreach ($orderList as $order) {
                $this->addOrder($order);
            }
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return ucfirst($this->firstName) . ' ' . ucfirst($this->lastName);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Add an order to the order collection
     *
     * @param Order $order
     */
    public function addOrder(Order $order)
    {
        $this->orders->push($order);
    }

    /**
     * @return int
     */
    public function getOrderCount(): int
    {
        return $this->orders->count();
    }

    /**
     * Get the lifetime Value for this Customer.
     *
     * @return float
     */
    public function getLifeTimeValue(): float
    {
        $lifeTimeValue = 0;
        /** @var Order $order */
        foreach ($this->orders as $order) {
            $lifeTimeValue += $order->getTotal();
        }
        return $lifeTimeValue;
    }
}