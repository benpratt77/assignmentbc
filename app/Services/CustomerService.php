<?php
declare(strict_types=1);

namespace App\Services;

use App\Customer;
use Bigcommerce\Api\Client;

class CustomerService
{
    /**
     * @var OrderService
     */
    protected $orderService;

    /**
     * CustomerService constructor.
     * @param OrderService $orderService
     */
    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * Get a list of Customers.
     *
     * @return Customer[]
     */
    public function getCustomers()
    {
        $customers = Client::getCustomers();
        $customerList = [];
        foreach ($customers as $customerDetails) {
            $customer = new Customer(
                $customerDetails->id,
                $customerDetails->first_name,
                $customerDetails->last_name,
                $this->orderService->getOrdersForCustomer($customerDetails->id)) ?:[];
            $customerList[] = $customer;
        }
        return $customerList;
    }

    /**
     * Get a Customer by Id.
     *
     * @param int $customerId
     * @return Customer
     */
    public function getCustomer(int $customerId): Customer
    {
        $customerDetails = Client::getCustomer($customerId);
        $customer = new Customer(
            $customerId,
            $customerDetails->first_name,
            $customerDetails->last_name,
            $this->orderService->getOrdersForCustomer($customerId) ?: []
        );
        return $customer;
    }
}