<?php
declare(strict_types=1);

namespace App\Services;

use App\Order;
use Bigcommerce\Api\Client;

class OrderService
{
    /**
     * @param int $customerId
     * @return Order[]
     */
    public function getOrdersForCustomer(int $customerId)
    {
        $filter = ["customer_id" => $customerId];
        $orders = Client::getOrders($filter)?:[];
        return array_map(function ($order) {
            $productCount = 0;
            if ($order->products) {
                $productCount = count($order->products);
            }
            return new Order(
                $order->id,
                new \DateTimeImmutable($order->date_created),
                floatval ($order->total_inc_tax),
                $productCount
            );
        }, $orders);
    }
}