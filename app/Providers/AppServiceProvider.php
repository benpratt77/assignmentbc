<?php

namespace App\Providers;

use App\Services\CustomerService;
use App\Services\OrderService;
use Bigcommerce\Api\Client;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (env('API_STORE_URL')) {
            Client::configure([
                'store_url' => env('API_STORE_URL'),
                'username' => env('API_USERNAME'),
                'api_key' => env('API_KEY'),
            ]);
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Register our OrderService.
        $this->app->bind('App\Services\OrderService', function ($app) {
            return new OrderService();
        });
        // Register our CustomerService.
        $this->app->bind('App\Services\CustomerService', function ($app) {
            return new CustomerService(new OrderService());
        });

    }
}
