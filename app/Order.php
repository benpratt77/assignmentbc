<?php
declare(strict_types=1);

namespace App;

class Order
{
    /** @var int $id */
    private $id;
    /**
     * The date this Order was created.
     * @var int $date
     */
    private $date;
    /**
     * The total Value of this Order.
     * @var int $total
     */
    private $total = 0;
    /**
     * A count of the amount of Products that were in this Order.
     * @var  int $productCount
     */
    private $productCount;

    /**
     *
     * @param int $id
     * @param \DateTimeImmutable $date
     * @param float $total
     * @param int $productCount
     */
    public function __construct(int $id, \DateTimeImmutable $date, float $total, int $productCount)
    {
        $this->id = $id;
        $this->date = $date;
        $this->total = $total;
        $this->productCount = $productCount;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }

    /**
     * @return float
     */
    public function getTotal(): float
    {
        return $this->total;
    }

    /**
     * @return int
     */
    public function getProductCount(): int
    {
        return $this->productCount;
    }
}